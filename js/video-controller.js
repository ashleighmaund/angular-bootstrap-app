videoApp.controller('VideoController', ['$scope', '$window', '$interval', '$http', function($scope, $window, $interval, $http) {

	$scope.videoDisplay = document.getElementById("happy-video");
	$scope.videoSource = $window.videoSource;
	$scope.titleDisplay = $window.titleDisplay;
	$scope.videoDescription = $window.videoDescription;
	$scope.videoPlaying = false;
	$scope.currentTime;
	$scope.totalTime;
	$scope.fromLeft = -1000;
	$scope.fromTop = -1000;
	$scope.halfVideoHeight = -1000;
	$scope.halfVideoWidth = -1000;
	$scope.isDragging = false;
	$scope.showOptions = false;
	$scope.playlist;

	$http.get('data/playlist.json').success(function(data) {
		$scope.playlist = data;
		console.log($scope.playlist);
	});

	$interval(function() {
		if (!$scope.isDragging) {
			var time = $scope.videoDisplay.currentTime;
			var duration = $scope.videoDisplay.duration;
			var width = time / duration * 100;
			var progress = document.getElementById('playerProgressMeter').offsetLeft + document.getElementById('playerProgressMeter').offsetWidth;
			$scope.fromLeft = (time / duration * progress) - 7;
		} else {
			$scope.fromLeft = document.getElementById('thumbBar').offsetLeft;
		};
		$scope.updateTimeOnPage();
	}, 100);

	$scope.togglePlay = function() {
		if($scope.videoDisplay.paused) {
			$scope.videoDisplay.play();
			$scope.videoPlaying = true;
			$('#playButton span').toggleClass("glyphicon-play", false);
			$('#playButton span').toggleClass("glyphicon-pause", true);
		} else {
			$scope.videoDisplay.pause();
			$scope.videoPlaying = false;
			$('#playButton span').toggleClass("glyphicon-pause", false);
			$('#playButton span').toggleClass("glyphicon-play", true);
		};
	};

	$scope.toggleMute = function() {
		if ($scope.videoDisplay.volume != 0) {
			$scope.videoDisplay.volume = 0;
			$('#muteButton span').toggleClass("glyphicon-volume-off", false);
			$('#muteButton span').toggleClass("glyphicon-volume-up", true);
		} else {
			$scope.videoDisplay.volume = 1.0;
			$('#muteButton span').toggleClass("glyphicon-volume-up", false);
			$('#muteButton span').toggleClass("glyphicon-volume-off", true);
		};
	};

	$scope.toggleDetails = function() {
		if ($scope.showOptions) {
			$scope.showOptions = false;
		} else {
			$scope.showOptions = true;
		};
	};

	$scope.videoSelected = function(index) {
		$scope.titleDisplay = $scope.playlist[index].title;
		$scope.videoDescription = $scope.playlist[index].description;
		$scope.videoSource = $scope.playlist[index].path;
		$scope.videoDisplay.load($scope.videoSource);
		$scope.videoPlaying = false;
		$('#playButton span').toggleClass("glyphicon-play", true);
		$('#playButton span').toggleClass("glyphicon-pause", false);
		$scope.showOptions = false;
	};

	$scope.initialisePlayer = function() {
		$scope.currentTime = 0;
		$scope.totalTime = 0;
		$scope.videoDisplay.addEventListener("timeupdate", $scope.updateTime, true);
		$scope.videoDisplay.addEventListener("loadedmetadata", $scope.updateData, true);
	};

	$scope.updateTime = function(event) {
		if (!$scope.videoDisplay.seeking) {
			$scope.currentTime = event.target.currentTime;
			if ($scope.currentTime == $scope.totalTime) {
				$scope.videoDisplay.pause();
				$scope.videoPlaying = false;
				$scope.currentTime = 0;
				$('#playButton span').toggleClass("glyphicon-play", true);
				$('#playButton span').toggleClass("glyphicon-pause", false);
			};
		};
	};

	$scope.updateData = function(event) {
		$scope.totalTime = event.target.duration;
	};

	$scope.updateTimeOnPage = function() {
		$scope.fromTop = document.getElementById('playerProgressMeter').offsetTop-2;
		$scope.halfVideoHeight = $scope.videoDisplay.offsetHeight/2 - 50;
		$scope.halfVideoWidth = $scope.videoDisplay.offsetWidth/2 - 50;
		if(!$scope.$$phase) {
			$scope.$apply();
		};
	};

	$scope.mouseMoving = function($event) {
		if ($scope.isDragging) {
			$("#thumbBar").offset({left:$event.pageX});
		};
	};

	$scope.dragStart = function($event) {
		$scope.isDragging = true;
	};

	$scope.dragStop = function($event) {
		if ($scope.isDragging) {
			$scope.videoSeek($event);
			$scope.isDragging = false;
		};
	};

	$scope.videoSeek = function($event) {
		var width = document.getElementById('playerProgressMeter').offsetWidth;
		var duration = $scope.videoDisplay.duration;
		var seconds = Math.round($event.pageX / width * duration);
		$scope.videoDisplay.currentTime = seconds;
	};

	$scope.initialisePlayer();

}]);