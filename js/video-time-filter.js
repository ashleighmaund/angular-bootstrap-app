videoApp.filter('timeFilter', function() {
	return function(seconds) {
		var hour = Math.floor(seconds / 3600);
		var minute = Math.floor(seconds / 60) % 60;
		var second = Math.floor(seconds) % 60;
		return hour + ":" + (minute < 10 ? "0" : "") + minute + ":" + (second < 10 ? "0" : "") + second;
	}
});