Mineo
=====

A small video player app dedicated to minion videos!

Tech Used
---------

Lovingly crafted with Angular and styled with Bootstrap

Running the App Locally...
------------------------------

Navigate to any folder in the command line

```
$ git clone https://bitbucket.org/ashleighmaund/angular-bootstrap-app/

$ cd angular-bootstrap-app

$ npm install
```

You may have to install node and bower before installing the dependencies.

... On Mac
----------

You can install HTTP-Server globally (then look under "... On Windows") or you can just run it with the following command:

``` $ ./node_modules/http-server/bin/http-server ```

Then in Google Chrome, navigate to "http://localhost:8080/" and the application should load!

... On Windows
--------------

Install http-server globally:

``` $ npm install -g http-server ```

And run HTTP-Server:

``` $ http-server ```

Then in Google Chrome, navigate to "http://localhost:8080/" and the application should load!